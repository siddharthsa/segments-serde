package com.zapr.adtech.deserializer;

import com.zapr.adtech.segments.Segment;
import com.zapr.adtech.segments.ZaprSegments;

import java.util.List;

/**
 * This is deserialization utils used by all the producers of Zapr Segments.
 * Every record represents a segment and corresponding set of users modelled
 * in a bitmap for their presence.
 *      segment_id : bitmap [isPresent]
 * This will be handled my maintaining separate indexes for each user corresponding
 * to bitmap index
 *
 * Created by siddharth on 4/29/16.
 */

public interface SegmentDeseriliazer<S> {

    /**
     * Given the segment id returns the {@link Segment} object
     *
     * @param segmentid
     * @return {@link Segment}
     */

    Segment getSegment(String segmentid);


    /**
     * gets the {@link ZaprSegments} objects from the source mentioned
     *
     * @param {@link S} source of serialized segments
     * @return {@link ZaprSegments}
     */

    ZaprSegments getAllSegments(S source);


    /**
     * This takes the array of segment ids and return the bitmap of indexes of
     * unique user ids
     *
     * @param segmentIds
     * @return {@link byte[]}
     */

    byte[] getUniqueUsersFromMultipleSegments(String[] segmentIds);

}
