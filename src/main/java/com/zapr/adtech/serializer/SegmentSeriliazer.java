package com.zapr.adtech.serializer;

import com.zapr.adtech.segments.Segment;

import java.util.List;

/**
 * This is serializer utils used by all the consumers of Zapr Segments.
 * Every record represents a segment and corresponding set of users modelled
 * in a bitmap for their presence.
 *      segment_id : bitmap [isPresent]
 * This will be handled my maintaining separate indexes for each user corresponding
 * to bitmap index
 *
 * Created by siddharth on 4/29/16.
 */

public interface SegmentSeriliazer<T> {

    /**
     * Given the segment id and the list of all user id this function returns the serializable
     * {@link Segment} object
     *
     * @param segmentid
     * @param users
     * @return {@link Segment}
     */
    Segment createSegment(String segmentid, List<String> users);


    /**
     * Takes the array of {@link Segment} objects and uploads to target
     *
     * @param segments
     */
    void uploadSegments(Segment[] segments, T target);


}
